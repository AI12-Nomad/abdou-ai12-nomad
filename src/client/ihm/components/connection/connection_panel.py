import pygame
from pygame import time
import pygame_gui
from config.config import Config
from UIComponents.Component import Component
from pygame_gui.elements import text


class ConnectionPanel(Component):

    def __init__(self, pygame_manager: pygame_gui.UIManager) -> None:
        self.pygame_manager = pygame_manager

        self.config = Config()
        self.set_width(self.config.get("monitor")['width'] // 2)
        self.set_height(self.config.get("monitor")['height'] // 2)
        self.set_pos_x(self.config.get("monitor")['height'] * 0.25)
        self.set_pos_y(self.config.get("monitor")['width'] * 0.25)

        self.render()

    def render(self) -> None:
        self.gui_element = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect((self.pos_x, self.pos_y), (self.width, self.height)),
            manager=self.pygame_manager, starting_layer_height=1)





